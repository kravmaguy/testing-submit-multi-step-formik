export const onSubmit = (
  values,
  { setSubmitting },
  history,
  path,
  dispatch
) => {
  switch (path) {
    case "/":
      return history.push("/step2");
    default: {
      dispatch({ type: "SAVE_FORM", values });
    }
  }
};
