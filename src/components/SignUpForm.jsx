import { Formik, Form, Field, ErrorMessage } from "formik";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { useHistory, useLocation } from "react-router";
import * as yup from "yup";
import { Switch, Route } from "react-router-dom";
import { onSubmit } from "./onSubmit";
import Navigation from "./Navigation";

export function SignUpForm() {
  const initialValues = useSelector((state) => state);
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();
  const path = location.pathname;

  const handleSubmit = (values, formikBag) => {
    onSubmit(values, formikBag, history, path, dispatch);
  };

  const validationSchema = yup.object().shape({
    email: yup.string().email().required(),
    name: yup.string().min(2),
    password: yup.string().min(2),
    location: yup.string().min(2),
  });

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={handleSubmit}
    >
      {({ values }) => (
        <Form>
          <Switch>
            <Route exact path="/">
              <StepOne />
            </Route>
            <Route path="/step2">
              <StepTwo values={values} />
            </Route>
          </Switch>
        </Form>
      )}
    </Formik>
  );
}

export function StepOne() {
  return (
    <>
      <h1>Please enter your info</h1>
      <label>
        Email
        <Field className="input" name="email" type="eamil" />
      </label>
      <ErrorMessage className="error" name="email" component="div" />
      <label>
        Name
        <Field className="input" name="name" type="text" />
      </label>
      <ErrorMessage className="error" name="name" component="div" />
      <label>
        Password
        <Field className="input" name="password" type="password" />
      </label>
      <ErrorMessage className="error" name="password" component="div" />
      <Navigation>Next</Navigation>
    </>
  );
}

export function StepTwo() {
  return (
    <>
      <h1>Please enter your location</h1>
      <label>
        Location
        <Field className="input" name="location" type="text" />
      </label>
      <ErrorMessage className="error" name="location" component="div" />
      <Navigation backLink="/">Submit</Navigation>
    </>
  );
}
