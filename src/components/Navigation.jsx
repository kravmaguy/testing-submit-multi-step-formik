import { Link } from "react-router-dom";

const Navigation = ({ backLink, children }) => {
  return (
    <div>
      {backLink && <Link to={backLink}>Back</Link>}
      <button type="submit">{children}</button>
    </div>
  );
};

export default Navigation;
