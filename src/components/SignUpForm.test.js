import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { SignUpForm } from "./SignUpForm";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { MemoryRouter } from "react-router";
import { reducer } from "../redux/reducer";
import * as api from "./onSubmit";

const initialValues = {
  email: "john@expample.com",
  name: "John",
  password: "test",
  location: "",
};

const StepTwo = () => {
  const store = createStore(reducer, initialValues);
  return (
    <MemoryRouter initialEntries={["/step2"]}>
      <Provider store={store}>
        <SignUpForm />
      </Provider>
    </MemoryRouter>
  );
};

const StepOne = () => {
  const store = createStore(reducer, initialValues);
  return (
    <MemoryRouter initialEntries={["/"]}>
      <Provider store={store}>
        <SignUpForm />
      </Provider>
    </MemoryRouter>
  );
};

const fn = api.onSubmit;
api.onSubmit = jest.fn();

describe("components/SignUpForm", () => {
  test("Final Data Submission", async () => {
    const { name, email, password } = initialValues;
    render(<StepTwo />);
    const location = "6679 beverly";
    userEvent.type(screen.getByLabelText(/location/i), location);
    userEvent.click(screen.getByText(/Submit/i));
    await waitFor(() => {
      expect(api.onSubmit).toHaveBeenCalled();
    });
    expect(api.onSubmit.mock.calls[0][0]).toEqual({
      email,
      name,
      password,
      location,
    });
    // api.onSubmit = fn;
  });
  test("Cannot be submitted with insufficient data", async () => {
    render(<StepTwo />);
    userEvent.click(screen.getByText(/Submit/i));
    await waitFor(() => {
      expect(api.onSubmit).not.toHaveBeenCalled();
    });
    api.onSubmit = fn;
  });

  test("can advance", async () => {
    render(<StepOne />);
    userEvent.click(screen.getByText(/next/i));
    await waitFor(() => {
      expect(screen.getByRole("heading", { level: 1 })).toHaveTextContent(
        /Please enter your location/i
      );
    });
  });
  test("can go back", async () => {
    render(<StepTwo />);
    userEvent.click(screen.getByText(/back/i));
    await waitFor(() => {
      expect(screen.getByRole("heading", { level: 1 })).toHaveTextContent(
        /Please enter your info/i
      );
    });
  });
  test("can not advance with insufficient data", async () => {
    render(<StepOne />);
    expect(await screen.findByRole("heading", { level: 1 })).toHaveTextContent(
      /Please enter your info/i
    );
    userEvent.click(screen.getByText(/next/i));
    expect(await screen.findByRole("heading", { level: 1 })).toHaveTextContent(
      /Please enter your info/i
    );
  });
});
