import { SignUpForm } from "./components/SignUpForm";
import { BrowserRouter as Router } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Router>
        <SignUpForm />
      </Router>
    </div>
  );
}

export default App;
